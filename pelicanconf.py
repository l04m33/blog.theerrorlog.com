#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Kay Z.'
SITENAME = 'The Error Log'
SITEURL = ''

PATH = 'content'
PATH_PAGES = ['pages']
STATIC_PATHS = ['img', 'css', 'js', 'dl']

OUTPUT_PATH = 'output'
DELETE_OUTPUT_DIRECTORY = True
YEAR_ARCHIVE_SAVE_AS = 'archives/{date:%Y}.html'
MONTH_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/{date:%m}.html'
DAY_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/{date:%m}/{date:%d}.html'

TIMEZONE = 'Asia/Hong_Kong'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 20

RELATIVE_URLS = True

PYGMENTS_RST_OPTIONS = {
    'cssclass': 'pgcss-highlight',
    'linenos': 'table',
    }

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {
            'linenums': True,
            'css_class': 'pgcss-highlight',
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}

PLUGIN_PATHS = ['plugins']
PLUGINS = ['thumbnailer']

# thumbnailer
IMAGE_PATH = 'img/pic'
THUMBNAIL_DIR = 'img/pic/thumbnails'
THUMBNAIL_SIZES = {
    'default': '400x?',
}
THUMBNAIL_KEEP_NAME = True
THUMBNAIL_KEEP_TREE = True
