Three.js 笔记 4：鼠标交互
#########################

:date: 2015-02-02 18:52
:tags: three.js, webgl
:category: Dev
:slug: threejs-note-4-mouse-interaction
:author: Kay Z.
:lang: zh

这里的“鼠标交互”并不是指 addEventListener 提供的交互——虽然也会用到——而
是另一个更具体的问题：鼠标没有3D坐标，怎么和三维空间里的物体交互？想象
一下，如果我们在3D视图上单击了一下鼠标，要怎么找到被单击的3D对象？

当然，我们可以自己用鼠标二维坐标 + 投影矩阵 + 3D对象的变换矩阵 + 3D对
象的几何信息，计算出鼠标到底点击在哪个对象上……或者，我们也可以用 Three.js
提供的 Raycaster_ .

.. _Raycaster: http://threejs.org/docs/#Reference/Core/Raycaster

.. PELICAN_END_SUMMARY

Ray Casting
===========

Raycaster 使用的方法正是 `Ray Casting`_ ，顾名思义就是从一点发射一束“光线”
——动感死光biubiubiu——然后看看这束“光线”在前进路线上会遇到什么东西。

乍看这似乎和鼠标交互没什么关系，但是当我们在用鼠标选择3D场景中的物体时，可
以想象也有一束“光线”从镜头所在的点出发，沿着鼠标所指的方向发射出去，那么这
一束光线一定会首先遇到鼠标方向上最靠近镜头的物体，一般情况下这也正是我们想
要用鼠标操作的物体。

.. _Ray Casting: http://en.wikipedia.org/wiki/Ray_casting

遥控纸飞机
==========

这里的例子沿用了 `上一篇笔记`_ 的场景，但是我们现在可以用鼠标来控制飘来飘去
的纸飞机了～我用了一个 **看不见** 的平面来“截获”镜头“发射”出来的“光线”，从
而得到纸飞机的目标位置。

.. _上一篇笔记: {filename}/threejs-note-3-simple-animation.rst

.. raw:: html

    <script src="js/lib/webgl/three.min.js"></script>
    <script src="js/lib/webgl/Detector.js"></script>
    <script src="js/lib/webgl/TrackballControls.js"></script>
    <script src="js/lib/webgl/routine.js"></script>
    <script>
        Picker = function (dom, camera, w, h) {
            this.raycaster = new THREE.Raycaster();
            this.camera = camera;
            this.mouse = new THREE.Vector2();

            var thiz = this;

            dom.addEventListener('mousemove', function (ev) {
                // calculate mouse position in normalized device coordinates
                // (-1 to +1) for both components

                var targetOffset = $(ev.target).offset(),
                    mouseX = ev.pageX - targetOffset.left,
                    mouseY = ev.pageY - targetOffset.top;

                thiz.mouse.x = ( mouseX / w ) * 2 - 1;
                thiz.mouse.y = - ( mouseY / h ) * 2 + 1;
            });

            this.intersects = function (objs) {
                this.raycaster.setFromCamera(this.mouse, this.camera);
                return this.raycaster.intersectObjects(objs);
            }
        }

        function initGeo (xsize, zsize) {
            var xmax = xsize / 2,
                xmin = -xmax,
                zmax = zsize / 2,
                zmin = -zmax;
            var geo = new THREE.Geometry();

            geo.vertices.push(
                new THREE.Vector3(xmin, 0, zmin),
                new THREE.Vector3(xmax, 0, zmin),
                new THREE.Vector3(xmax, 0, zmax),
                new THREE.Vector3(xmin, 0, zmax)
            );

            geo.faces.push(
                new THREE.Face3(0, 1, 3),
                new THREE.Face3(2, 3, 1)
            );

            geo._edges = {
                '0:1': new THREE.Vector2(0, 1),
                '1:3': new THREE.Vector2(1, 3),
                '0:3': new THREE.Vector2(0, 3),
                '2:3': new THREE.Vector2(2, 3),
                '1:2': new THREE.Vector2(1, 2)
            };

            geo._face2edges = [
                ['0:1', '1:3', '0:3'],
                ['2:3', '1:3', '1:2']
            ];

            return geo;
        }

        function subdivide (geo) {
            var edgeVertices = {};
            var e, v1, v2, vm;

            for (var i in geo._edges) {
                e = geo._edges[i];
                v1 = geo.vertices[e.x];
                v2 = geo.vertices[e.y];
                vm = v1.clone();
                vm.add(v2).multiplyScalar(0.5);
                edgeVertices[i] = geo.vertices.length;
                geo.vertices.push(vm);
            }

            var face, faceEdges,
                newFaces = [],
                newFace2Edge = [],
                newEdges = {},
                subFace, newFacesData;

            function addEdge(a, b, newEdges) {
                var v1 = Math.min(a, b),
                    v2 = Math.max(a, b),
                    key = v1 + ':' + v2;

                newEdges[key] = new THREE.Vector2(v1, v2);
                return key;
            }

            for (i = 0; i < geo._face2edges.length; i++) {
                face = geo.faces[i];
                faceEdges = geo._face2edges[i];

                newFacesData = [
                    [
                        face.a,
                        edgeVertices[faceEdges[0]],
                        edgeVertices[faceEdges[2]]
                    ],
                    [
                        face.b,
                        edgeVertices[faceEdges[1]],
                        edgeVertices[faceEdges[0]]
                    ],
                    [
                        face.c,
                        edgeVertices[faceEdges[2]],
                        edgeVertices[faceEdges[1]]
                    ],
                    [
                        edgeVertices[faceEdges[0]],
                        edgeVertices[faceEdges[1]],
                        edgeVertices[faceEdges[2]]
                    ]
                ];

                for (var j = 0; j < newFacesData.length; j++) { 
                    subFace = new THREE.Face3(
                        newFacesData[j][0],
                        newFacesData[j][1],
                        newFacesData[j][2]
                    );
                    newFaces.push(subFace);
                    newFace2Edge.push([
                        addEdge(subFace.a, subFace.b, newEdges),
                        addEdge(subFace.b, subFace.c, newEdges),
                        addEdge(subFace.a, subFace.c, newEdges)
                    ]);
                }
            }

            geo.faces = newFaces;
            geo._edges = newEdges;
            geo._face2edges = newFace2Edge;

            return geo;
        }

        function mirror (mesh) {
            var mirrorGeo = mesh.geometry.clone();
            var mirrorMatrix = new THREE.Matrix4();

            mirrorMatrix.set(
                -1, 0, 0, 0,
                 0, 1, 0, 0,
                 0, 0, 1, 0,
                 0, 0, 0, 1
            );
            mirrorGeo.applyMatrix(mirrorMatrix);

            return (new THREE.Mesh(mirrorGeo, mesh.material));
        }

        function rotateMeshes (meshes, speed, delta, size, scene) {
            for (var i = 0; i < meshes.length; i++) {
                meshes[i].position.add(speed.clone().multiplyScalar(delta));
            }

            var limit = size / 4;
            var distVector = meshes[0].position.clone().sub(new THREE.Vector3(0, 0, 0));
            var distSq = distVector.lengthSq();

            if (distSq >= (limit * limit) && meshes.length < 2) {
                var mirrorMesh = mirror(meshes[0]);
                mirrorMesh.position.x -= (size - Math.sqrt(distSq));
                meshes.push(mirrorMesh);
                scene.add(mirrorMesh);
            }

            if (distSq >= (limit * 3 * limit * 3) && meshes.length >= 2) {
                scene.remove(meshes.shift());
            }
        }

        function drawPaperPlane() {
            var geo = new THREE.Geometry();

            geo.vertices.push(
                new THREE.Vector3(0, 0.4, -0.5),
                new THREE.Vector3(0.5, 0.4, 0.5),
                new THREE.Vector3(0.05, 0.4, 0.5),
                new THREE.Vector3(-0.5, 0.4, 0.5),
                new THREE.Vector3(-0.05, 0.4, 0.5),
                new THREE.Vector3(0, 0.0, 0.5)
            );

            var colorBright = new THREE.Color(1, 1, 1),
                colorDark = new THREE.Color(0.8, 0.8, 0.8);

            geo._colors = [
                colorBright,
                colorBright,
                colorDark,
                colorBright,
                colorDark,
                colorBright
            ];

            geo.faces.push(
                new THREE.Face3(0, 2, 1),
                new THREE.Face3(0, 3, 4),
                new THREE.Face3(0, 5, 4),
                new THREE.Face3(0, 2, 5)
            );

            for (var i = 0; i < geo.faces.length; i++) {
                geo.faces[i].vertexColors = [
                    geo._colors[geo.faces[i].a],
                    geo._colors[geo.faces[i].b],
                    geo._colors[geo.faces[i].c]
                ];
            }

            var mat = new THREE.MeshBasicMaterial({
                vertexColors: THREE.VertexColors
            });
            mat.side = THREE.DoubleSide;

            return (new THREE.Mesh(geo, mat));
        }

        function initThree (domParent, outw, outh) {
            if (!Detector.webgl) {
                var msg = Detector.getWebGLErrorMessage();
                domParent.appendChild(msg);
            }

            var scene = new THREE.Scene();
            var camera = new THREE.PerspectiveCamera(75, outw / outh, 0.1, 1000);

            var renderer = new THREE.WebGLRenderer({antialias: true});
            renderer.setSize(outw, outh);
            renderer.setClearColor(0x000000, 1);
            domParent.appendChild(renderer.domElement);

            var controls = new THREE.TrackballControls(camera, renderer.domElement);

            return {
                scene: scene,
                camera: camera,
                renderer: renderer,
                controls: controls
            };
        }
    </script>

    <p><div id="threejs-output"></div></p>

    <script>
        (function () {
            function drawFaces (scene, xsize, zsize, subTimes) {
                var geo = initGeo(xsize, zsize);

                for (var t = 0; t < subTimes; t++) {
                    subdivide(geo);
                }

                geo._colors = [];
                for (var v = 0; v < geo.vertices.length; v++) {
                    geo.vertices[v].y = Math.random() * 2 - 1;
                    var colorScale = (geo.vertices[v].y + 2) / 4 + 0.25;
                    var colorScale2 = (geo.vertices[v].y + 2) / 4;
                    geo._colors.push(new THREE.Color(colorScale, colorScale, colorScale2));
                }
                for (var f = 0; f < geo.faces.length; f++) {
                    geo.faces[f].vertexColors = [
                        geo._colors[geo.faces[f].a],
                        geo._colors[geo.faces[f].b],
                        geo._colors[geo.faces[f].c]
                    ]
                }

                var mat = new THREE.MeshBasicMaterial({
                    vertexColors: THREE.VertexColors
                });
                mat.side = THREE.DoubleSide;

                var mesh = new THREE.Mesh(geo, mat);

                scene.add(mesh);

                return mesh;
            }

            var target = new THREE.Vector3(4, 0, 0);

            function movePlane (plane, delta) {
                if (target.y !== 0 || target.z !== 0) {
                    var distVector = (new THREE.Vector3()).subVectors(
                        plane._orig_position,
                        plane.position
                    );

                    var mouseDistVector = (new THREE.Vector3()).subVectors(target, plane.position);

                    var planeSpeed = mouseDistVector.clone().multiplyScalar(0.1);

                    plane.position.add(planeSpeed);

                    plane.rotation.z = (distVector.z / 0.3 * (Math.PI / 12));
                }

                return plane;
            }

            var t = initThree(
                    document.getElementById('threejs-output'), 
                    533, 300
                );

            t.controls.noRotate = true;
            t.controls.noZoom = true;
            t.controls.noPan = true;
            t.controls.noRoll = true;

            t.renderer.setClearColor(0xfffff7);
            t.scene.fog = new THREE.FogExp2(0xfffff7, 0.1, 1000);
            t.camera.position.set(4.35, 1.45, 0);
            t.camera.lookAt(0, 0, 0);

            var plane = drawPaperPlane();
            plane.position.set(4, 1.25, 0);
            plane.rotation.set(0, Math.PI / 2, 0);
            plane.scale.set(0.05, 0.05, 0.05);
            plane._orig_position = plane.position.clone();
            t.scene.add(plane);

            var catcherMat = new THREE.MeshBasicMaterial({
                    color: 0x0000ff,
                    wireframe: true
                }),
                rayCatcher = new THREE.Mesh(initGeo(64, 64), catcherMat); 

            rayCatcher.rotation.z = Math.PI / 2;
            rayCatcher.position.set(4, 1.25, 2);
            t.scene.add(rayCatcher);

            var picker = new Picker(
                    t.renderer.domElement, t.camera,
                    t.renderer.domElement.width, t.renderer.domElement.height
                );

            t.renderer.domElement.addEventListener('mousemove', function () {
                var intersects = picker.intersects([rayCatcher]);

                if (intersects.length > 0) {
                    target.y = intersects[0].point.y;
                    target.z = intersects[0].point.z;
                }
            });

            var meshes = [ drawFaces(t.scene, 80, 64, 6) ],
                clock = new THREE.Clock(),
                speed = new THREE.Vector3(1, 0, 0);

            function render () {
                requestAnimationFrame(render);

                var delta = clock.getDelta();

                rotateMeshes(meshes, speed, delta, 80, t.scene);
                movePlane(plane, delta);
                t.controls.update();

                t.renderer.render(t.scene, t.camera);
            }

            render();
        })();
    </script>
