Title: Shinpachi参上
Date: 2014-10-20 12:00
Category: Projects
Tags: python, html5, http
Slug: introducing-shinpachi
Author: Kay Z.
Lang: zh

之前[研究asyncio][1]模块的时候，顺便写了个[叫Shinpachi的小工具][2]，放在这
写篇东西介绍一下XD

[1]: {filename}/2014-06-04-asyncio-in-python-3.md
[2]: https://shinpa.ch

<!-- PELICAN_END_SUMMARY -->

## Shinpachi要解决的问题 ##

有没有试过为了在手机上抓一个包，特地在PC上装了Wireshark和整套开发环境？就算
不是移动客户端，有没有试过要在生产环境抓包，但是服务器和客户端都不在你手里？
Shinpachi正是为这些场景而生的。

它是一个特殊的HTTP代理服务器，只要将手机上——或者其他什么设备——的代理地址设置
成Shinpachi，你就能在Shinpachi的控制台上看到所有TCP流量。

我在[这里][3]部署了一个实例，不过是要用OAuth帐号登录的。因为毕竟是代理服务器，
很容易被滥用，所以还是意思意思，给保护一下……

[3]: https://shinpa.ch

## 实现细节 ##

实际上很简单，这里分成前端和后端说明一下。

Shinpachi的**前端**是一个HTML页面，通过websocket与后端（服务器）保持长连接。前
端经由这个连接给后端发送指令，而后端会返回捕捉到的数据流。后端返回的是原始
HTTP协议，所以这个HTML页面上还包含一个用Javascript写的HTTP协议解析器，解析后
再将结果显示在页面上。

前端的DOM操作用的是[Zepto][4]；语法高亮则是[highlight.js][5].

至于**后端**，当然就是Python+[aiohttp][6]啦，另外还有[jinja2][7]和[zeromq][8]……
很多实现细节我已经在[上一篇文章][9]里交代了，这里就不啰嗦了。值得一提的是后端
用aiohttp实现了很多平台的OAuth逻辑，看哪天把它们单独拿出来变成一个package呗～

[4]: http://zeptojs.com/
[5]: https://highlightjs.org/
[6]: https://github.com/KeepSafe/aiohttp
[7]: http://jinja.pocoo.org/
[8]: http://zeromq.org/
[9]: {filename}/2014-06-04-asyncio-in-python-3.md

## 源码 ##

托管在[Github上][10]，欢迎来fork :)

[10]: https://github.com/l04m33/shinpachi
