封面图
######

:date: 2014-10-31 17:40
:tags: photo
:category: Photos
:slug: banner-image
:author: Kay Z.
:lang: zh

附送一张失焦版本。这张照片其实是从我家阳台上拍的:)

.. figure:: /img/pic/thumbnails/default/DSC00880.png
    :alt: City Night
    :target: {static}/img/pic/DSC00880.png

.. figure:: /img/pic/thumbnails/default/DSC00884.png
    :alt: City Night Defocused
    :target: {static}/img/pic/DSC00884.png
