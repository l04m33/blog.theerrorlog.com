Distant Mist
############

:date: 2019-07-25 10:00
:tags: photo
:category: Photos
:slug: distant-mist
:author: Kay Z.

There's *always* this distant mist around where I live.
What do people living *in* the mist see, I wonder?

.. figure:: /img/pic/thumbnails/default/DSC01616.png
    :alt: Distant Mist 1
    :target: {static}/img/pic/DSC01616.png

.. figure:: /img/pic/thumbnails/default/DSC01622.png
    :alt: Distant Mist 2
    :target: {static}/img/pic/DSC01622.png
