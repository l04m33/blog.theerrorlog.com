空中看晚霞
##########

:date: 2015-03-25 11:58
:tags: photo
:category: Photos
:slug: watching-sunset-in-the-sky
:author: Kay Z.
:lang: zh

最近那个“日食航班”很火嘛，据说那位机长为了看日食故意开着飞机在半
路上绕了很多圈，导致降落的时候晚点了，不过貌似乘客反而很感激他就
是了……

这里的当然不是日食照片，而是我在年初去玩的时候在深航的 A320 上拍
的——第一次带着相机坐傍晚的航班。

.. figure:: /img/pic/thumbnails/default/DSC01250.png
    :alt: Sunset
    :target: {static}/img/pic/DSC01250.png

.. figure:: /img/pic/thumbnails/default/DSC01253.png
    :alt: Sunset 2
    :target: {static}/img/pic/DSC01253.png
