A Taste of the TMK Keyboard Firmware
####################################

:date: 2019-01-03 21:40
:tags: keyboard, tmk, hhkb
:category: Utilities
:slug: a-taste-of-the-tmk-keyboard-firmware
:author: Kay Z.

Everything is hackable nowadays, and keyboards are no
exception. Some time ago when I was searching for a
replacement for my good ol' HHKB, I got fascinated by the
concept of programmable keyboards. I worked with my
keyboards all the time, and kept changing the key
bindings in all kinds of software to suit my weird taste.
With a programmable keyboard, I can take those weird
bindings of mine anywhere with me. How awesome is that?

More searches. One of the results led me to the `HHKB
Alternative Controller`_ page. To sum it up, some
brilliant people designed a controller board precisely
for the HHKB, and it's powered by the same chip
(ATmega32U4) on Arduino boards. In other words, that
thing is programmable from inside out. It's even
open-sourced! But I'm not an iron-and-soldering guy,
instead, I bought one from the designer, and all I had
to do were loosening some screws and connecting the
controller board to my old keyboard. Now I have a
*programmable* HHKB! 

.. _HHKB Alternative Controller: https://geekhack.org/index.php?topic=12047.0

To Actually Program the Keyboard
================================

You need the `TMK keyboard firmware`_. The `QMK
firmware`_ is more like the de facto standard for
programming keyboards nowadays, because it supports
more keyboards and have more fancy features, but I
simply followed the instructions given by the designer
of the HHKB alt. controller, and used TMK instead [1]_.
This is really a random choice, but TMK is powerful
enough that I lack the motivation to actully switch to
QMK.

.. [1] To make it clear, the designer of the HHKB
       alt. controller and the current maintainer of
       the TMK firmware is exactly the same person.

That said, it's easy enough to follow the documentation
scattered across the ``tmk_core/doc`` directory, and
soon discover your own way of customizing your firmware.
If you're looking for more advanced/more interesting
stuff, there are designated hook APIs
(``tmk_core/doc/hook.txt``). Moreover, you can override
core functions annotated with ``__attribute__((weak))``
for *extra fun*, but this is by no means documented so
be careful.

.. _TMK keyboard firmware: https://github.com/tmk/tmk_keyboard
.. _QMK firmware: https://github.com/qmk/qmk_firmware

An Opinionated Config
=====================

`My customization`_ was based on the stock config came
with the alt. controller board, which was identical with
stock HHKBs. I basically just added new layers to suit my
needs, and here's a brief list of the extra features:

* A dedicated layer for mouse keys and cursor movement
  keys (arrow keys, Pg Up/Dn, Home/End etc.) in Vim
  style (i.e. H/J/K/L for going left/down/up/right). I
  used this layer a lot, for browsing through web pages
  and navigating inside some dumb command lines.

* Dynamic macros. This is the same feature found in the
  QMK firmware. I almost regretted implementing this
  myself, since it required too much effort, and I ended
  up rarely using it. I'd say my implementation is quite
  hacky too, but I don't feel like investing more time
  to improve it.

* A layer filled with ``KC_NO``, to emulate a *locked*
  keyboard. My toddler would sometimes reach for the
  keyboard while I was away from the desk, and locking
  the screen seems not enough.

* A layer for `Penti Chorded Keyboard`_ mode. This mode
  enables one-handed typing, while my typing fingers can
  always stay in their home positions. This is quite
  useful when I need to do some quick-and-dirty editing
  but don't want to move my other hand away from the
  mouse. The Topre switches are a bit too stiff for
  *chords* requiring three or more keys though.

* `Space Cadet Shift`_ keys. The left/right Shift keys
  are configured to act like this: when tapped, emit a
  left/right parenthesis; when held down with other keys,
  act as normal Shift keys. This behavior may interfere
  with some IMEs [2]_, where they use the shift keys to
  switch input modes. I solved this by simply have
  *vanilla* Shift keys mapped in another layer.

* Automatically insert right parentheses. There's an
  *Auto Paren* mode that I made. When activated, tapping
  the left Shift key would insert a pair of parentheses,
  and then put the cursor in between; tapping the right
  Shift key would just push the cursor one character
  away to the right. This makes all those dumb text
  editors look a bit smarter.

* The Enter key also acts as right Control when held
  down. Symmetry achieved!

.. [2] Input Method Engines

With all these features and some excessive debug
messages, I used up *all* the ROM space available on
that tiny controller chip. At least I can keep my hands
off the firmware for now, until I feel like hacking
again. And after being taught these new tricks, maybe my
good ol' HHKB can hold up to some more thocking years.

.. _My customization: https://github.com/l04m33/dot-files/blob/master/tmk_hhkb/unimap_l04m33.c
.. _Penti Chorded Keyboard: https://software-lab.de/penti.html
.. _Space Cadet Shift: http://stevelosh.com/blog/2012/10/a-modern-space-cadet/#shift-parentheses
