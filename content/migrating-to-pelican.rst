迁移到Pelican
#############

:date: 2014-10-31 10:44
:tags: blog, pelican
:category: Blog
:slug: migrating-to-pelican
:author: Kay Z.
:lang: zh

其实吧，我是个很懒的人，而且不懂Ruby. 所以呀，应付 Jekyll_ 什么的对我来
说真的压力很大。所以呀，我一咬牙把这整个Blog迁移到 Pelican_ 上去了。好处
是我终于可以自己做主题和写插件了；坏处则是，所有文章的URL都变了，所以
所有的Disqus评论都木有了……

.. _Jekyll: http://jekyllrb.com/
.. _Pelican: http://blog.getpelican.com/

.. PELICAN_END_SUMMARY


迁移步骤
========

原本Jekyll版博客用的是Markdown，还好Pelican也支持用Markdown写内容，只是
文件头的元信息格式不大一样（见 `Pelican的文档`_ ），所以内容迁移其实还
好，写点脚本外加手工修改一番就OK了。比较麻烦的是 **网站主题** ，之前的
Jekyll版其实没什么主题可言，我只是自己写了两个layout而已——而Pelican是提
倡将layout和内容完全分开的，所以需要补一个独立的 `主题`_ 。

Pelican主题使用的 `Jinja2模板`_ 和Jekyll的 `Liquid模板`_ 在语法上非常相
似，所以 `新主题`_ 基本上是在原本的Jekyll layout基础上修改而来的，外加
一些美化。

具体来说，我是这样干的：

1. 先写一个Pelican模板： `index.html`_ ；
2. 用这个模板和原本的CSS样式、Javascript文件，搭一个简单的Pelican主题；
3. 修改原本的Markdown内容，改掉文件元信息和 `内部链接的格式`_ ；
4. 用修改后的Markdown内容和上面的半成品主题生成一遍Blog；
5. 根据生成的效果再去写 `article.html`_ 和其他模板；
6. 各种润色，大功告成。

.. _Pelican的文档: http://docs.getpelican.com/en/3.4.0/content.html#file-metadata
.. _主题: http://docs.getpelican.com/en/3.4.0/themes.html
.. _Jinja2模板: http://jinja.pocoo.org/
.. _Liquid模板: http://liquidmarkup.org/
.. _新主题: https://github.com/l04m33/theerrorlog-theme.git
.. _index.html: https://github.com/l04m33/theerrorlog-theme/blob/master/templates/index.html
.. _article.html: https://github.com/l04m33/theerrorlog-theme/blob/master/templates/article.html
.. _内部链接的格式: http://docs.getpelican.com/en/3.4.0/content.html#linking-to-internal-content

其他问题
========

Jekyll会自动提取文章的第一个段落作为摘要，但是Pelican比较笨，只会去提取指
定长度的内容，甚至不惜把文章里的表格拦腰扯断……网上有个 `提取文章摘要的插件`_ ，
可以让作者自己指定摘要的边界，但是这个插件 `不支持在摘要里使用内部链接`_ ，
所以我又 `自己写了一个`_ ……

.. _提取文章摘要的插件: https://github.com/getpelican/pelican-plugins/tree/master/summary
.. _不支持在摘要里使用内部链接: https://github.com/getpelican/pelican-plugins/issues/314
.. _自己写了一个: https://gist.github.com/l04m33/7e93b0bb3ca0cfc6f2d0
