从 Gnome Shell 切换到 Awesome WM
################################

:date: 2015-12-09 20:50
:tags: awesome, desktop
:category: Utilities
:slug: switching-from-gnome-shell-to-awesome-wm
:author: Kay Z.
:lang: zh

很久很久以前我为了装装逼用过一下 StumpWM_ ，但是日复一日地按 Ctrl-t 实在是太
可怕了，为了调整窗口布局我的小指头简直要断掉……当然我知道快捷键可以在配置里改，
只是 StumpWM 的配置也没有很友善就是了。这之后我幼小的心灵就对各种 Tiling WM
留下了阴影，直到发现了 `Awesome WM`_ ……

.. _StumpWM: https://stumpwm.github.io/
.. _Awesome WM: http://awesome.naquadah.org/

.. PELICAN_END_SUMMARY

Awesome WM 的杀手级 Feature
===========================

1. 配置简单（用 Lua ）；

2. 不区分 tiling 和 floating 窗口（处理 GIMP 之类的程序和对话框之类的不会像
   StumpWM 那么悲剧）；

3. 预置各种窗口布局，而且调整起来非常简单；

4. 不像别的 WM 提供虚拟桌面，而是给窗口打 tag ，非常灵活。

安装 Awesome WM
===============

在 Fedora 下安装 Awesome 很容易： ``dnf install awesome``

成功安装后在 GDM （用户登录）界面的 Session 菜单下选择 “Awesome”， 就能进入
Awesome WM.

从 Gnome Shell 切换到 Awesome WM 需要处理的东西
===============================================

大概从 Fedora 20 开始我都在用 Gnome Shell ，在切换到 Awesome 的时候发现自己
已经中毒太深，不小心依赖了一大堆乱七八遭的东西：

1. 输入法;

2. Nautilus （文件浏览、关联文件类型）；

3. Network Manager （管理网络连接）；

4. gnome-keyring-daemon （储存 ssh 密钥, 否则需要频繁输入密码）；

5. gnome-terminal （虚拟终端）；

6. 屏保和休眠管理；

7. 自动识别外接屏幕；

8. 时钟、音量控制、屏幕亮度控制、剩余电量显示等我以前根本都没意识到的功能……

这些东西很大一部分是 Gnome 自带的，而且以默认配置启动的 Awesome 只在屏幕右上
角提供了一个简单的时钟，要离开 Gnome 就只能找替代品了。

输入法
------

原本 Gnome Shell 的输入法界面是内置的，基本上不需要配置就能用。而 Awesome 只
是一个窗口管理器，连 IME 是什么都不知道，所以要自己动手……我选了似乎是目前维
护得最好的 ibus_ ，在 Fedora 下用拼音输入法只要安装 ``ibus-libpinyin`` 就可
以了，然后在 Awesome 的 rc.lua 中加入以下语句，让 Awesome 启动的时候顺便运
行 ibus-daemon ：

.. code-block:: lua

    awful.util.spawn("ibus-daemon -d -x -r -n awesome")

另外为了让各种图形库都能识别 ibus ，还需要在 ``$HOME/.profile`` 中加入这几个
环境变量：

.. code-block:: sh

    export XMODIFIERS=@im=ibus
    export GTK_IM_MODULE=ibus
    export QT_IM_MODULE=ibus

干完这些事情后重新登录进入 Awesome ， ibus 的图标应该就出现在屏幕右上角了，
进一步的设置可以通过点击图标用图形界面完成。

.. figure:: {static}/img/ibus-icon.png
    :alt: ibus icon
    :target: {static}/img/ibus-icon.png

.. _ibus: https://github.com/ibus/ibus

文件浏览、关联文件类型
----------------------

强烈推荐 ranger_ . 这是个工作在命令行下的文件浏览器，好用到爆，甚至能显示图
片预览，懂得自动通过 MIME 信息判断文件类型并且用你的默认程序打开。同样是
``dnf install ranger`` 即可。

当然，ranger 只能处理直接从文件浏览器打开文件时的类型关联，我还不清楚从其他
GUI程序——像 Chrome 浏览器的下载列表之类——打开文件时的关联是怎么配置的；貌似
我自己很少这样直接打开文件所以也没有深究。

.. figure:: {static}/img/ranger-screenshot.png
    :alt: ranger screenshot
    :target: {static}/img/ranger-screenshot.png

.. _ranger: https://github.com/hut/ranger

管理网络连接
------------

Gnome Shell 有自己的网络连接管理面板，但实际上还是调用了 NetworkManager_ 的
接口，而 NetworkManager 又很贴心地提供了命令行控制程序 nmcli_ . 我的网络环
境一般比较稳定，没必要特地在 GUI 上显示网络状态，一般的网络操作在命令行下就
能完成。

列出可用的无线网络，列表中带星号（*）的是当前已连接的网络：

.. code-block:: sh

    nmcli dev wifi

连接到某个新的无线网络：

.. code-block:: sh

    nmcli dev wifi connect <SSID>

连接到已保存的网络：

.. code-block:: sh

    nmcli conn up <Name or UUID>

.. _NetworkManager: https://wiki.gnome.org/Projects/NetworkManager
.. _nmcli: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Networking_Guide/sec-Using_the_NetworkManager_Command_Line_Tool_nmcli.html

储存 SSH 密钥
-------------

传统上这个事情是由 `ssh-agent`_ 来做的，但是 Gnome 环境用
`gnome-keyring-daemon`_ 取代了 ssh-agent. 为了和 Gnome 环境“划清界限”，我重
新启用了 ssh-agent ，方法是使用 `这个 zsh 插件`_ ——因为我的 shell 是 zsh ——在
每次进入 zsh 的时候检查 ssh-agent 有没有启动并且设置好相应的环境变量。

当然这个方法只能支持命令行下的 ssh 客户端——不过反正我的 ssh 操作都是命令行
下的所以没差了……

.. _ssh-agent: https://en.wikipedia.org/wiki/Ssh-agent
.. _gnome-keyring-daemon: https://en.wikipedia.org/wiki/GNOME_Keyring
.. _这个 zsh 插件: https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/ssh-agent/ssh-agent.plugin.zsh

虚拟终端
--------

以前一直是 Gnome 党所以用的是 gnome-terminal ，感觉还是不错的，除了有时候觉
得太慢。在 Awesome 的默认配置里 Ctrl-Enter 这个快捷键是“打开虚拟终端”，而这
个打开的虚拟终端默认是 xterm. xterm 的默认外观实在是伤眼睛所以我曾一度想要
回到 gnome-terminal 的怀抱，不料调查了一番 xterm 之后却又仿佛打开了新世界的
大门。大家感受一下配置前和配之后 xterm 的外观差异：

.. figure:: {static}/img/xterm-not-configured.png
    :alt: xterm not configured
    :target: {static}/img/xterm-not-configured.png

    配置前

.. figure:: {static}/img/xterm-configured.png
    :alt: xterm configured
    :target: {static}/img/xterm-configured.png

    配置后

具体配置方法是将选项写在 ``$HOME/.Xresources`` 文件中，然后执行：

.. code-block:: sh

    xrdb ~/.Xresources

另外 xterm 可以自定义快捷键，而且居然还能自定义鼠标双击、三击、四击甚至五击
时的行为：

.. code-block:: txt

    ! “三击”时选择以空格字符分隔的内容
    XTerm*on3Clicks: regex [^ ]+

    ! “四击”时选择整行内容
    XTerm*on4Clicks: regex .+

    ! 三组快捷键，从上到下功能分别是：
    ! 按住 Shift 点击鼠标，用 Firefox 打开已选择的 URL
    ! 按住 Alt 点击鼠标，用 Chrome 打开已选择的 URL
    ! 释放鼠标左键时，将“PRIMARY”寄存器的内容复制到“CLIPBOARD”寄存器
    XTerm*VT100*translations: #override \
            Shift ~Meta <Btn1Up>: exec-formatted("firefox '%t'", PRIMARY) \n\
            ~Shift Meta <Btn1Up>: exec-formatted("google-chrome '%t'", PRIMARY) \n\
            ~Shift ~Meta <Btn1Up>: select-end(PRIMARY, CLIPBOARD, CUT_BUFFER0)

最后 ``select-end`` 那行是为了在 xterm 和其他 GUI 程序之间复制文本。 xterm
的复制粘贴操作与其他程序中 Ctrl-c Ctrl-v 不同，用鼠标选中的文本会直接被复制
到一个叫 “PRIMARY” 的寄存器内，然后按下 Shift-Insert ，这个寄存器的内容就会被
粘贴到光标位置；而其他大部分程序在 Ctrl-v 时粘贴的是另一个 “CLIPBOARD” 寄存器
的内容，所以在 xterm 中选定文本时，需要将 “PRIMARY” 寄存器的内容复制到
“CLIPBOARD”，这样选中的内容才能被粘贴到其他程序中……

其他的基本上都是外观选项，有兴趣的同学可以参观一下 `我的配置文件`_ 。

这篇文章介绍了一些好用的选项和 hack ： `Hidden gems of xterm`_

.. _我的配置文件: https://github.com/l04m33/dot-files/blob/master/.Xresources
.. _Hidden gems of xterm: http://lukas.zapletalovi.com/2013/07/hidden-gems-of-xterm.html

屏保和休眠管理
--------------

这个其实好办， Xorg 下的屏保有很多选择，而且大都附带休眠管理的配置。我挑了
“老牌”的 xscreensaver ，在 Awesome 的 rc.lua 配置文件里加入：

.. code-block:: lua

    awful.util.spawn("xscreensaver -no-splash")

xscreensaver 的配置选项也很多，不过我们可以通过 xscreensaver-demo 命令用图
形界面进行设置，所有选项都保存在 ``$HOME/.xscreensaver`` 文件中。

外接屏幕
--------

偶尔要把电脑接到大屏幕上；之前的屏幕管理貌似是 Gnome Shell 自己做的，外接
屏幕插上就能自动识别，但是 Awesome 并没有做这个事情，还是要我们自己来。

在 Xorg 下启用多个屏幕的方法不止一种： xrandr 、Xinerama 、Nvidia 的
TwinView …… Xinerama 是旧的 X 标准，只能将所有屏幕合并为一个大桌面；
TwinView 是 Nvidia 自家技术，只有 N 卡能用； xrandr 则用的是新的 RandR 扩
展，能动态改变屏幕配置，所以这里用的是 xrandr 命令。

不带任何参数运行 xrandr 命令可以看到所有屏幕的连接状态和参数：

.. code-block:: txt

    Screen 0: minimum 8 x 8, current 1366 x 768, maximum 32767 x 32767
    LVDS1 connected 1366x768+0+0 (normal left inverted right x axis y axis) 293mm x 164mm
       1366x768      60.01*+
       1280x720      60.00  
       1024x768      60.00  
       1024x576      60.00  
       960x540       60.00  
       800x600       60.32    56.25  
       864x486       60.00  
       640x480       59.94  
       720x405       60.00  
       680x384       60.00  
       640x360       60.00  
    DP1 disconnected (normal left inverted right x axis y axis)
    HDMI1 disconnected (normal left inverted right x axis y axis)
    VGA1 disconnected (normal left inverted right x axis y axis)
    VIRTUAL1 disconnected (normal left inverted right x axis y axis)

带星号（*）那行是当前分辨率，带加号（+）那行则是最佳分辨率。

启用外接屏幕，并将这个屏幕的桌面放在主屏幕的右侧：

.. code-block:: sh

    xrandr --output HDMI1 --right-of LVDS1 --auto

这样打开的外接屏幕会显示一个独立的桌面，而且默认的 Awesome 配置会给这个桌面
分配独立的 tags. 可以使用 ModKey-Ctrl-j 访问下一屏幕， ModKey-Ctrl-k 则是上
一个。

关闭外接屏幕：

.. code-block:: sh

    xrandr --output HDMI1 --off

xrandr 命令所作的修改在退出 X 图形环境之后就会消失，想要保存屏幕设置的话需
要直接编辑 ``/etc/X11/xorg.conf.d/*.conf`` .

除了图像外，我们也可以将 **音频** 输出到 HDMI 接口。最简单的方法是安装并执
行 pavucontrol 程序，在 “Configuration” 选项卡的 “Profile” 列表里选择 HDMI
输出。

其他小东西
----------

1. 时钟：我在用 Awesome 默认显示在右上角的那个；

2. 音量控制：使用键盘上的多媒体按键，在 rc.lua 文件的 globalkeys 里加入

.. code-block:: lua

   awful.key({ }, "XF86AudioRaiseVolume",
       function ()
           awful.util.spawn_with_shell("amixer sset Master 5%+")
       end),
   awful.key({ }, "XF86AudioLowerVolume",
       function ()
           awful.util.spawn_with_shell("amixer sset Master 5%-")
       end),
   awful.key({ }, "XF86AudioMute",
       function ()
           awful.util.spawn_with_shell("amixer sset Master toggle")
       end)

3. 屏幕亮度控制：用 xbacklight 命令

.. code-block:: sh

    xbacklight -set 50  # 亮度设置在 50%
    xbacklight -inc 10  # 亮度增加 10%
    xbacklight -dec 10  # 亮度减少 10%
    xbacklight -get     # 获取当前亮度

4. 电池电量显示：使用 vicious 库（``dnf install vicious``）的电量 widget，
   在 rc.lua 文件里加入

.. code-block:: lua

    local vicious = require("vicious")
    ...
    local batwidget = wibox.widget.textbox()
    vicious.register(batwidget, vicious.widgets.bat, ' <span color="' .. widgetcolor .. '">$2%</span>', 60, "BAT0")
    ...
    right_layout:add(batwidget)

锦上添花
========

设置主题和桌面背景
------------------

为 Awesome 自定义主题很简单。将 ``/usr/share/awesome/themes/default`` 复制
到 ``$HOME/.config/awesome/themes/`` 目录下——当然除了 default 你也可以选其
他主题——然后按照自己的喜好这里删删那里改改就可以了。

要套用某个主题，只需要在 rc.lua 里加上

.. code-block:: lua

    local beautiful = require("beautiful")
    beautiful.init("<path to your theme.lua file>")

要更改桌面背景的话，修改 theme.lua 文件中的 ``theme.wallpaper`` 变量即可。

另外，由于 Awesome 的配置文件其实就是 lua 程序，你可以动态生成桌面背景的文
件名，甚至是动态生成背景图片本身，就看你想怎么玩……

添加各种 Widget
---------------

Awesome 社区里有很多完全用 lua 实现的 widget 库，像是 `vicious`_ 和
`lain`_ ，一般挑一个自己喜欢的就够了。我的 widget 配置很简单，感兴趣的同学
欢迎来参观 `我的 rc.lua`_ .

.. _vicious: https://github.com/Mic92/vicious
.. _lain: https://github.com/copycat-killer/lain
.. _我的 rc.lua: https://github.com/l04m33/dot-files/blob/master/awesome/rc.lua

启用 Composite Manager
----------------------

X 环境有个很好玩的设定：窗口管理器和桌面特效是完全独立的两个东西，我们可以
给最简陋的窗口管理器配以最华丽丽的特效，又或者完全反过来。很多窗口管理器兼
职做了特效管理——比方说以前很火的 compiz ——但是我们的 Awesome WM 不管这块。
想要在 Awesome WM 下获得透明窗口之类的特效，通常的方法是另外运行一个
Composite Manager.

这其中最老字号的就是 xcompmgr 了，但是在我的机器上 xcompmgr 的窗口阴影完全
出不来，所以我找了另外一个新点的程序，叫 compton_ . 实际上 compton 是从
xcompmgr fork 出来的，在 xcompmgr 的基础上加了一些不错的功能。

在 rc.lua 中加入以下语句，就可以让你的桌面瞬间充满质感：

.. code-block:: lua

    awful.util.spawn("compton -c -C -t-4 -l-4 -r4 -o.75 -f -D7 -I.07 -O.07 -b")

.. _compton: https://github.com/chjj/compton

上截图
======

.. figure:: /img/pic/thumbnails/default/awesome-wm-screenshot.png
    :alt: awesome wm screenshot
    :target: {static}/img/pic/awesome-wm-screenshot.png
