Three.js 笔记 3：简单动画
#########################

:date: 2015-01-28 10:27
:tags: three.js, webgl
:category: Dev
:slug: threejs-note-3-simple-animation
:author: Kay Z.
:lang: zh

迄今为止我们绘制的3D场景都是静止的，死气沉沉啊有木有？现在我们可以
给它们注入点生命力了。

.. PELICAN_END_SUMMARY

怎么画动画
==========

如果有看过 `之前`_ 的 `笔记`_ ，应该有同学留意到像这样的代码：

.. code-block:: javascript

    function render() {
        requestAnimationFrame(render);
        controls.update();
        renderer.render(scene, camera);
    }

    render();

没错 render 函数就是我们画动画的地方。因为我们调用了 requestAnimationFrame_
函数将 render 注册为动画渲染回调函数，所以浏览器在认为画面需要重绘的时候就会
调用 render. 我们可以在 render 中微调各个对象的位置、形状、贴图坐标等，那么
连续的渲染结果就能构成动画了。

不过使用 requestAnimationFrame 有个微妙的问题： render 函数的调用时间间隔不
是固定的，为了让动画速度保持恒定，我们还需要根据每次调用的不同时间间隔计算
动画微调的幅度。 Three.js 提供了方便的 Clock_ 类用于支持此类操作：

.. code-block:: javascript

    var clock = THREE.Clock();

    function render() {
        requestAnimationFrame(render);

        var delta = clock.getDelta();
        // .... 根据 delta 大小决定动画幅度 ....

        controls.update();
        renderer.render(scene, camera);
    }

这里取得的 delta 就是以秒为单位的间隔时间。

.. _之前: {filename}/threejs-note-1-basic-scene-setup.rst
.. _笔记: `上一篇笔记`_
.. _requestAnimationFrame: https://developer.mozilla.org/en-US/docs/Web/API/window.requestAnimationFrame
.. _Clock: http://threejs.org/docs/#Reference/Core/Clock

绵延不绝的地形
==============

让我们从上次画的那个其实不怎么像的地形开始。我想要一个“低空飞过”的
动画效果，但是——如果大家有去看源码就会知道——这个地形 Mesh 只有
64x64 那么小，别说飞了，就是走也能走出去，除非我们一直原地画圈圈。

于是，我这样投机取巧了一下：

.. raw:: html

    <script src="js/lib/webgl/three.min.js"></script>
    <script src="js/lib/webgl/Detector.js"></script>
    <script src="js/lib/webgl/TrackballControls.js"></script>
    <script src="js/lib/webgl/routine.js"></script>
    <script>
        function initGeo (xsize, zsize) {
            var xmax = xsize / 2,
                xmin = -xmax,
                zmax = zsize / 2,
                zmin = -zmax;
            var geo = new THREE.Geometry();

            geo.vertices.push(
                new THREE.Vector3(xmin, 0, zmin),
                new THREE.Vector3(xmax, 0, zmin),
                new THREE.Vector3(xmax, 0, zmax),
                new THREE.Vector3(xmin, 0, zmax)
            );

            geo.faces.push(
                new THREE.Face3(0, 1, 3),
                new THREE.Face3(2, 3, 1)
            );

            geo._edges = {
                '0:1': new THREE.Vector2(0, 1),
                '1:3': new THREE.Vector2(1, 3),
                '0:3': new THREE.Vector2(0, 3),
                '2:3': new THREE.Vector2(2, 3),
                '1:2': new THREE.Vector2(1, 2)
            };

            geo._face2edges = [
                ['0:1', '1:3', '0:3'],
                ['2:3', '1:3', '1:2']
            ];

            return geo;
        }

        function subdivide (geo) {
            var edgeVertices = {};
            var e, v1, v2, vm;

            for (var i in geo._edges) {
                e = geo._edges[i];
                v1 = geo.vertices[e.x];
                v2 = geo.vertices[e.y];
                vm = v1.clone();
                vm.add(v2).multiplyScalar(0.5);
                edgeVertices[i] = geo.vertices.length;
                geo.vertices.push(vm);
            }

            var face, faceEdges,
                newFaces = [],
                newFace2Edge = [],
                newEdges = {},
                subFace, newFacesData;

            function addEdge(a, b, newEdges) {
                var v1 = Math.min(a, b),
                    v2 = Math.max(a, b),
                    key = v1 + ':' + v2;

                newEdges[key] = new THREE.Vector2(v1, v2);
                return key;
            }

            for (i = 0; i < geo._face2edges.length; i++) {
                face = geo.faces[i];
                faceEdges = geo._face2edges[i];

                newFacesData = [
                    [
                        face.a,
                        edgeVertices[faceEdges[0]],
                        edgeVertices[faceEdges[2]]
                    ],
                    [
                        face.b,
                        edgeVertices[faceEdges[1]],
                        edgeVertices[faceEdges[0]]
                    ],
                    [
                        face.c,
                        edgeVertices[faceEdges[2]],
                        edgeVertices[faceEdges[1]]
                    ],
                    [
                        edgeVertices[faceEdges[0]],
                        edgeVertices[faceEdges[1]],
                        edgeVertices[faceEdges[2]]
                    ]
                ];

                for (var j = 0; j < newFacesData.length; j++) { 
                    subFace = new THREE.Face3(
                        newFacesData[j][0],
                        newFacesData[j][1],
                        newFacesData[j][2]
                    );
                    newFaces.push(subFace);
                    newFace2Edge.push([
                        addEdge(subFace.a, subFace.b, newEdges),
                        addEdge(subFace.b, subFace.c, newEdges),
                        addEdge(subFace.a, subFace.c, newEdges)
                    ]);
                }
            }

            geo.faces = newFaces;
            geo._edges = newEdges;
            geo._face2edges = newFace2Edge;

            return geo;
        }

        function mirror (mesh) {
            var mirrorGeo = mesh.geometry.clone();
            var mirrorMatrix = new THREE.Matrix4();

            mirrorMatrix.set(
                -1, 0, 0, 0,
                 0, 1, 0, 0,
                 0, 0, 1, 0,
                 0, 0, 0, 1
            );
            mirrorGeo.applyMatrix(mirrorMatrix);

            return (new THREE.Mesh(mirrorGeo, mesh.material));
        }

        function rotateMeshes (meshes, speed, delta, size, scene) {
            for (var i = 0; i < meshes.length; i++) {
                meshes[i].position.add(speed.clone().multiplyScalar(delta));
            }

            var limit = size / 4;
            var distVector = meshes[0].position.clone().sub(new THREE.Vector3(0, 0, 0));
            var distSq = distVector.lengthSq();

            if (distSq >= (limit * limit) && meshes.length < 2) {
                var mirrorMesh = mirror(meshes[0]);
                mirrorMesh.position.x -= (size - Math.sqrt(distSq));
                meshes.push(mirrorMesh);
                scene.add(mirrorMesh);
            }

            if (distSq >= (limit * 3 * limit * 3) && meshes.length >= 2) {
                scene.remove(meshes.shift());
            }
        }

        function drawPaperPlane() {
            var geo = new THREE.Geometry();

            geo.vertices.push(
                new THREE.Vector3(0, 0.4, -0.5),
                new THREE.Vector3(0.5, 0.4, 0.5),
                new THREE.Vector3(0.05, 0.4, 0.5),
                new THREE.Vector3(-0.5, 0.4, 0.5),
                new THREE.Vector3(-0.05, 0.4, 0.5),
                new THREE.Vector3(0, 0.0, 0.5)
            );

            var colorBright = new THREE.Color(1, 1, 1),
                colorDark = new THREE.Color(0.8, 0.8, 0.8);

            geo._colors = [
                colorBright,
                colorBright,
                colorDark,
                colorBright,
                colorDark,
                colorBright
            ];

            geo.faces.push(
                new THREE.Face3(0, 2, 1),
                new THREE.Face3(0, 3, 4),
                new THREE.Face3(0, 5, 4),
                new THREE.Face3(0, 2, 5)
            );

            for (var i = 0; i < geo.faces.length; i++) {
                geo.faces[i].vertexColors = [
                    geo._colors[geo.faces[i].a],
                    geo._colors[geo.faces[i].b],
                    geo._colors[geo.faces[i].c]
                ];
            }

            var mat = new THREE.MeshBasicMaterial({
                vertexColors: THREE.VertexColors
            });
            mat.side = THREE.DoubleSide;

            return (new THREE.Mesh(geo, mat));
        }

        function initThree (domParent, outw, outh) {
            if (!Detector.webgl) {
                var msg = Detector.getWebGLErrorMessage();
                domParent.appendChild(msg);
            }

            var scene = new THREE.Scene();
            var camera = new THREE.PerspectiveCamera(75, outw / outh, 0.1, 1000);

            var renderer = new THREE.WebGLRenderer({antialias: true});
            renderer.setSize(outw, outh);
            renderer.setClearColor(0x000000, 1);
            domParent.appendChild(renderer.domElement);

            var controls = new THREE.TrackballControls(camera, renderer.domElement);

            return {
                scene: scene,
                camera: camera,
                renderer: renderer,
                controls: controls
            };
        }
    </script>

    <p><div id="threejs-output-1"></div></p>

    <script>
        (function () {
            var t = initThree(
                    document.getElementById('threejs-output-1'), 
                    400, 300
                );
            t.camera.position.set(0, 70, 20);
            t.camera.lookAt(new THREE.Vector3(0, 0, 0));

            drawCoords(t.scene);

            var geo = initGeo(64, 64);
            subdivide(geo);

            var mat = new THREE.MeshBasicMaterial({ color: 0xffffffff, wireframe: true }),
                meshes = [new THREE.Mesh(geo, mat)],
                clock = new THREE.Clock(),
                speed = new THREE.Vector3(8, 0, 0);

            t.scene.add(meshes[0]);

            function render () {
                requestAnimationFrame(render);

                var delta = clock.getDelta();

                rotateMeshes(meshes, speed, delta, 64, t.scene);
                t.controls.update();

                t.renderer.render(t.scene, t.camera);
            }

            render();
        })();
    </script>

也就是说，移动的是地形，而不是我们的视角（视角一直静止在原点附近），而且
当地形快要罩不住原点的时候，我们生成一个 **地形的镜像** ，将它放在地形移
动方向的后方。因为是镜像，所以能和原本的地形 Mesh “无缝连接”。然后，在视
角距离原本的地形 Mesh 足够远的时候，反正也看不到了，我们就可以将原来那个
地形移除以节省资源。

因为 Three.js 没有提供直接生成镜像的接口，所以这里我们对 Geometry 对象使
用了 `Object3D.applyMatrix(...)`_ ，直接对几何结构进行矩阵变换，变换矩阵
是这个样子的：

.. code-block:: javascript

    var mirrorMatrix = new THREE.Matrix4();
    mirrorMatrix.set(
        -1, 0, 0, 0,
         0, 1, 0, 0,
         0, 0, 1, 0,
         0, 0, 0, 1
    );

可见只是将 X 轴坐标反转而已。

.. _Object3D.applyMatrix(...): http://threejs.org/docs/#Reference/Core/Object3D.applyMatrix

纸飞机
======

为了体现场景的宏大（？），我决定在镜头前面画一只小小的纸飞机。为什么是纸
飞机？因为只有4个面很好画……

.. raw:: html

    <p><div id="threejs-output-2"></div></p>
    <script>
        (function () {
            var t = initThree(
                    document.getElementById('threejs-output-2'), 
                    400, 300
                );
            t.camera.position.set(1, 1, 1);
            t.camera.lookAt(new THREE.Vector3(0, 0, 0));

            drawCoords(t.scene);
            var planeMesh = drawPaperPlane();
            t.scene.add(planeMesh);

            function render () {
                requestAnimationFrame(render);
                t.controls.update();
                t.renderer.render(t.scene, t.camera);
            }

            render();
        })();
    </script>

这里我用了 MeshBasicMaterial_ 的 vertexColors 选项，让材质颜色由每个顶点
单独确定，于是可以画出点简单的阴影效果——看过 `上一篇笔记`_ 源码的同学应该
也注意到了，地形的阴影也是这样搞出来的。

.. _MeshBasicMaterial: http://threejs.org/docs/#Reference/Materials/MeshBasicMaterial
.. _上一篇笔记: {filename}/threejs-note-2-drawing-3d-objects.rst

所有东西放在一起
================

为了让场景稍微好看一点，我给这里的 Scene_ 加了一个 FogExp2_ ：

.. _Scene: http://threejs.org/docs/#Reference/Scenes/Scene
.. _FogExp2: http://threejs.org/docs/#Reference/Scenes/FogExp2

.. raw:: html

    <p><div id="threejs-output-3"></div></p>
    <script>
        (function () {
            function drawFaces (scene, xsize, zsize, subTimes) {
                var geo = initGeo(xsize, zsize);

                for (var t = 0; t < subTimes; t++) {
                    subdivide(geo);
                }

                geo._colors = [];
                for (var v = 0; v < geo.vertices.length; v++) {
                    geo.vertices[v].y = Math.random() * 2 - 1;
                    var colorScale = (geo.vertices[v].y + 2) / 4 + 0.25;
                    var colorScale2 = (geo.vertices[v].y + 2) / 4;
                    geo._colors.push(new THREE.Color(colorScale, colorScale, colorScale2));
                }
                for (var f = 0; f < geo.faces.length; f++) {
                    geo.faces[f].vertexColors = [
                        geo._colors[geo.faces[f].a],
                        geo._colors[geo.faces[f].b],
                        geo._colors[geo.faces[f].c]
                    ]
                }

                var mat = new THREE.MeshBasicMaterial({
                    //color: 0x0000ff,
                    //wireframe: true,
                    vertexColors: THREE.VertexColors
                });
                mat.side = THREE.DoubleSide;

                var mesh = new THREE.Mesh(geo, mat);

                scene.add(mesh);

                return mesh;
            }

            function movePlane (plane, delta) {
                if (plane._speed === undefined) {
                    plane._speed = (new THREE.Vector3(
                        0,
                        Math.random() - 0.5,
                        Math.random() - 0.5
                    )).normalize().multiplyScalar(Math.random() * 0.5 + 0.25);
                }

                var distVector = (new THREE.Vector3()).subVectors(
                    plane._orig_position,
                    plane.position
                );

                if (distVector.lengthSq() >= 0.09) {
                    var randomScale = 1;
                        newSpeed = distVector.clone().normalize().add(
                            new THREE.Vector3(
                                0,
                                randomScale * Math.random() - randomScale / 2,
                                randomScale * Math.random() - randomScale / 2
                            )
                        ).normalize().multiplyScalar(Math.random() * 0.5 + 0.25);
                    plane._speed = newSpeed;
                }

                var distScale = Math.min(0.15, Math.max(0.01, (0.3 - distVector.length()) / 0.3));

                plane.position.add(plane._speed.clone().multiplyScalar(delta * distScale));

                plane.rotation.z = (distVector.z / 0.3 * (Math.PI / 6));
                return plane;
            }

            var t = initThree(
                    document.getElementById('threejs-output-3'), 
                    533, 300
                );
            t.renderer.setClearColor(0xfffff7);
            t.scene.fog = new THREE.FogExp2(0xfffff7, 0.1, 1000);
            t.camera.position.set(4.35, 1.45, 0);
            t.camera.lookAt(0, 0, 0);

            var plane = drawPaperPlane();
            plane.position.set(4, 1.25, 0);
            plane.rotation.set(0, Math.PI / 2, 0);
            plane.scale.set(0.05, 0.05, 0.05);
            plane._orig_position = plane.position.clone();
            t.scene.add(plane);

            var meshes = [ drawFaces(t.scene, 80, 64, 6) ],
                clock = new THREE.Clock(),
                speed = new THREE.Vector3(1, 0, 0);

            function render () {
                requestAnimationFrame(render);

                var delta = clock.getDelta();

                rotateMeshes(meshes, speed, delta, 80, t.scene);
                movePlane(plane, delta);
                t.controls.update();

                t.renderer.render(t.scene, t.camera);
            }

            render();
        })();
    </script>
