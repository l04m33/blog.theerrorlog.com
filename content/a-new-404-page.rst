新的404页面
###########

:date: 2015-03-21 15:13
:tags: 404, three.js, webgl
:category: Blog
:slug: a-new-404-page
:author: Kay Z.
:lang: zh

前几天在别人的站上看到 Nginx 默认的 404 页面，想起我自己这里也是一样
用的逗逼默认页，就来了兴致自己做一个，有兴趣的同学可以到 `这里`_ 围观:)

我在页面上直接用了 Three.js examples_ 里的特效代码，但是在不同的浏览器
窗口大小下使用的后期处理效果是不一样的。

.. _这里: /pages/404.html
.. _examples: https://github.com/mrdoob/three.js/tree/master/examples/js/postprocessing
