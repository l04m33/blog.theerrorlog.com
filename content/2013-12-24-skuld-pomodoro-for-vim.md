Title: Skuld：Vim下的番茄工作法
Date: 2013-12-24 12:00
Category: Projects
Tags: vim, 番茄工作法
Slug: skuld-pomodoro-for-vim
Author: Kay Z.
Lang: zh

最近心血来潮疯狂试用[各种][1][番茄][2][软件][3]，结果因为本人太花心所以用
一个扔一个，然后（省略若干内心独白和动机分析）决定自己写，以show off本人
扎实的编程功底。

[1]: https://github.com/codito/gnome-shell-pomodoro
[2]: https://play.google.com/store/apps/details?id=net.phlam.android.clockworktomato
[3]: http://alloyteam.github.io/AlloyTimer/

<!-- PELICAN_END_SUMMARY -->

## 需求 ##

1. 自动提醒和记时；我不要每隔半小时上个发条什么的，尽管番茄工作法的作者觉
   得上发条很爽；
2. 记录任务内容和完成任务消耗的番茄个数；光这点市面上的番茄软件就没一个做
   得好的，Android下[发条番茄钟][2]的日志功能挺好，但是没有记录任务内容。

其中**第二点**真的很重要，如果只知道我一天干了几个番茄，但是这些番茄的内
容却不知所踪，我凭什么做计划，凭什么改进工作效率嘛……很奇怪竟然所有软件都
无视了这个需求。你可以认为市面上的软件都只单纯地是个**钟**，只负责计时，
记录任务应该由别的东西完成；但是那些有装模作样记录了任务的软件们，你们的
导出功能敢再弱点吗？难道全都要我自己复制粘贴？难道你们的用户不可能有每天
做统计的习惯？

## 设计 ##

这么个小东西有什么好设计的……作为一名光荣的[装B的程序员][4]，我要把它做成
Vim插件，所以“设计过程”基本上就是在和Vim的“feature”搏斗的过程。

[4]: http://www.geekpark.net/read/view/194170

### Feature #1: Vim不提供timer ###

Vim里从来没有timer这个概念。其实也对，一个文本编辑器要timer来干什么，而且
还是一个模式化编辑器，timer被触发的时候行为是不好定义的。但是还好Vim可以
集成Python，这是一门完整的程序语言，随便你拿来干什么，包括自己写一个timer.

不过话说回来，一个文本编辑器要集成Python来干什么呢？谁知道呢……

### Feature #2: Vim的Python接口不能跨线程调用 ###

好吧，既然选了Python来做实现，最直接的当然是另外跑一个Python线程来计时。
问题是[网上][5][流传][6]说Vim里搞多线程调用Vim内部接口是找死，我就乖乖放
弃了。有人提到可以用`+clientserver`特性，通过进程间通信解决，试了下竟然有
效，就直接拿这个方法来用了。不过貌似因为`+clientserver`是通过X进行通信的，
Fedora下自带的`vim`就没有编入这个特性，只有带图形界面的`gvim`有。

[5]: http://vim.1045645.n5.nabble.com/Can-vimscript-be-multithreaded-td1193376.html
[6]: http://stackoverflow.com/questions/10386040/asynchronize-vim-script

### Featrue #3: Vim是个文本编辑器 ###

线程模型有了，UI方面（“方面”这个词很屌有没有），我的设想是用一个固定
buffer来暂存任务列表，输入和整理任务等等操作可以在这个buffer里做。问题是
任务列表的形式有点难搞。最自然的格式是一行一任务，但是这样好像会显得太随
便，而且Vim是个文本编辑器，用户在行与行之间干什么都行，格式太严格的话貌似
又会发生很多意料之外的情况……

然后我选了一行一个任务的简单格式。说不定以后再写个语法分析器，用户输入完
提示哪里有语法错误什么的——不过现在还是算了。

## 成品 ##

[在Github上][7]，小名`Skuld`，还加了一些语法高亮和快捷键什么的以提升本插
件的逼格。

[7]: https://github.com/l04m33/vim-skuld

![Skuld Screenshot](https://raw.github.com/l04m33/img/master/vim-skuld-screenshot.png)

