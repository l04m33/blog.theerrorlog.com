Artemis I
#########

:date: 2022-11-16 23:25
:tags: space, nasa, artemis
:category: Life
:slug: artemis-one
:author: Kay Z.

Artemis I was launched today at 1:47:44 am EST. It's the first step
of the Artemis project, which is literally an odyssey that would
eventually send astronauts to that neighboring red planet.

Artemis I is mainly a testing vessel. It's going to the moon and will
lay the foundation of future crewed moon missions. They'll then build
a moon base as a relay for subsequent Mars missions.

Ground breaking space missions like this always give me a strange
sensation. Maybe it's because the rocket launch scene from *5
Centimeters per Second* by Makoto Shinkai.

.. figure:: /img/5-centimeters-per-second-rocket-launch.jpg
    :alt: Rocket Launch Scene from 5 Centimeters per Second
    :target: {static}/img/5-centimeters-per-second-rocket-launch.jpg

    The rocket launch scene. Copyright CoMix Wave Films.

In that scene, Kanae tried to confess to Takaki but failed. While
Kanae was crying, a rocket lifted off from the nearby Tanegashima
Space Center. The characters' fierce emotions were suddenly dwarfed
by the magnificent launch scene in the setting sun.

We're so capable as a race, yet we're so isolated and helpless as
individuals.

We're always lonely, but we never stopped trying to reach out, either
as a race or as individuals.

When the SLS [1]_ launched, I was helping my daughter with her
homework. And a piece of happy deep autum sunshine painted our
balcony bright gold.

.. [1] Space Launch System, the launch vhiecle for Artemis I.
