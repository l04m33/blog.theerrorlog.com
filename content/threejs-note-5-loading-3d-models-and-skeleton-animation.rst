Three.js 笔记 5：载入 3D 模型和骨骼动画
#######################################

:date: 2015-02-12 10:21
:tags: three.js, webgl
:category: Dev
:slug: threejs-note-5-loading-3d-models-and-skeleton-animation
:author: Kay Z.
:lang: zh

好了我们到目前为止已经把 Three.js 的基础摸了一遍，大概没漏什么了吧……
但，是！像 `这种`_ 屌爆了的动画，难道所有模型都是用一行行的程序画出来
的吗？怎么可能 ╮(￣.￣)╭ ……

.. _这种: http://www.ro.me/

.. PELICAN_END_SUMMARY

准备工作
========

这次我们会直接使用现成的模型： `这台BF-109`_ （ `许可协议`_ ）。模型是
.3ds 格式，而且不带任何动画数据，所以我们需要：

1. 将模型转换为 Three.js 能处理的格式；
2. 给这台小灰机加入骨骼动画。

Three.js 有其特有的 `模型数据格式`_ ，但说白了其实就是 JSON . 貌似这个
东西最近开发速度很快所以经常有变动，这里以旧的 3.x 格式为准。

将其他格式转换为 Three.js 格式最方（pian）便（yi）的方法是用 `Blender`_ ，
因为 Three.js 源码里就提供了导出模型的 `Blender 插件`_ 。而且 Blender 本身
是个强大的 3D 建模和动画工具，我们可以顺便用它加入骨骼动画。

鉴于我们的重点是 Three.js 而不是 Blender ，我在这里直接放上已经加入骨骼
动画的 `.blend 文件`_ 和转换后的 `JSON 模型`_ 。

需要注意的是，这里使用的 Three.js 版本是 **r70** ，但是 Blender 插件用的
是 **r69** 版本，因为 r70 的 Blender 插件变化非常大，而且导出的动画在 r70
版本的 Three.js 中播放不正常，我这还没找到解决方法……另外使用 Blender 插件
导出模型的时候， **一定要** 选择 “Bones”、“Skinning”、“Skeleton animation”
这三个选项，否则骨骼动画不会被导出。

.. _这台BF-109: http://artist-3d.com/free_3d_models/dnm/model_disp.php?uid=3591
.. _许可协议: http://interartcenter.net/free-clip-art/copyrights.html
.. _模型数据格式: https://github.com/mrdoob/three.js/wiki/JSON-Model-format-3
.. _Blender: http://blender.org/
.. _Blender 插件: https://github.com/mrdoob/three.js/tree/master/utils/exporters/blender
.. _.blend 文件: {static}/dl/bf109e.blend
.. _JSON 模型: {static}/dl/bf109e_threejs.zip

JSONLoader
==========

Three.js 提供的 `JSONLoader`_ 可以以异步方式载入 3D 模型的所有数据，包括几
何结构、材质和贴图等，当然还有动画数据，并且提供了简单的载入进度管理。

.. _JSONLoader: http://threejs.org/docs/#Reference/Loaders/JSONLoader

以前面的模型为例，最简单的载入过程是这样的：

.. code-block:: javascript

    var loader = new THREE.JSONLoader();
    loader.load('bf109e/bf109e.json', function (geometry, materials) {
        model = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));
        scene.add(model);
    });

可见载入过程得到的结果是一个几何结构和一组材质，我们需要做的只是将它们组合为
一个 Mesh 对象。

执行结果如下（模型和贴图挺大，载入可能会比较慢）：

.. raw:: html

    <script src="js/lib/webgl/three.min.js"></script>
    <script src="js/lib/webgl/Detector.js"></script>
    <script src="js/lib/webgl/TrackballControls.js"></script>
    <script src="js/lib/webgl/routine.js"></script>
    <script>
        function addMainLight(scene) {
            var light = new THREE.PointLight(0xffffff, 1);
            light.position.set(5, 5, 0);
            scene.add(light);

            light = new THREE.PointLight(0xffffff, 1);
            light.position.set(-5, -5, 0);
            scene.add(light);

            light = new THREE.AmbientLight(0xffffff);
            scene.add(light);
        }

        function addEnvMap(scene) {
            var urls = [
                'dl/bf109e/0004.png',
                'dl/bf109e/0002.png',
                'dl/bf109e/0006.png',
                'dl/bf109e/0005.png',
                'dl/bf109e/0001.png',
                'dl/bf109e/0003.png'
            ];

            var cubemap = THREE.ImageUtils.loadTextureCube(urls);
            cubemap.format = THREE.RGBFormat;

            // following code from https://github.com/mrdoob/three.js/blob/master/examples/webgl_materials_cubemap.html
            var shader = THREE.ShaderLib[ "cube" ];
            shader.uniforms[ "tCube" ].value = cubemap;

            var material = new THREE.ShaderMaterial({
                fragmentShader: shader.fragmentShader,
                vertexShader: shader.vertexShader,
                uniforms: shader.uniforms,
                depthWrite: false,
                side: THREE.BackSide
            });

            var skybox = new THREE.Mesh(new THREE.BoxGeometry(1000, 1000, 1000), material);
            scene.add(skybox);
        }

        function drawParticles(scene, pcount, sx, sy, sz) {
            var par_count = pcount,
                spread_x = sx,
                spread_y = sy,
                spread_z = sz,
                par_geo = new THREE.Geometry(),
                par_mat = new THREE.PointCloudMaterial({
                    size: 1.5,
                    map: THREE.ImageUtils.loadTexture('dl/bf109e/star.png'),
                    blending: THREE.AdditiveBlending,
                    transparent: true,
                    depthTest: false
                });

            for (var i = 0; i < par_count; i++) {
                var p = new THREE.Vector3(
                    Math.random() * spread_x - spread_x / 2,
                    Math.random() * spread_y - spread_y / 2,
                    Math.random() * spread_z - spread_z / 2
                );

                //p.velocity = new THREE.Vector3(0, -Math.random() * 0.5, -Math.random());
                p.velocity = new THREE.Vector3(0, 0, -Math.random() * 100);

                par_geo.vertices.push(p)
            }

            var par_sys = new THREE.PointCloud(par_geo, par_mat);
            scene.add(par_sys);

            return par_sys;
        }

        function moveParticles(par_sys, sx, sy, sz, delta) {
            var par_count = par_sys.geometry.vertices.length;
            var vtx;
            for (var i = 0; i < par_count; i++) {
                vtx = par_sys.geometry.vertices[i];

                var delta_vel = vtx.velocity.clone();
                delta_vel.multiplyScalar(delta);
                vtx.add(delta_vel);

                if (vtx.x > sx / 2) {
                    vtx.setX(vtx.x - sx);
                } else {
                    if (vtx.x < - sx / 2) {
                        vtx.setX(vtx.x + sx);
                    }
                }
                if (vtx.y > sy / 2) {
                    vtx.setY(vtx.y - sy);
                } else {
                    if (vtx.y < - sy / 2) {
                        vtx.setY(vtx.y + sy);
                    }
                }
                if (vtx.z > sz / 2) {
                    vtx.setZ(vtx.z - sz);
                } else {
                    if (vtx.z < - sz / 2) {
                        vtx.setZ(vtx.z + sz);
                    }
                }
            }

            par_sys.geometry.verticesNeedUpdate = true;
        }
    </script>

    <p><div id="threejs-output-1"></div></p>

    <script>
        (function () {
            var t = initThree(
                    document.getElementById('threejs-output-1'), 
                    400, 300
                );
            t.renderer.setClearColor(0xcccccc);
            t.camera.position.set(1, 1, 1);
            t.camera.lookAt(new THREE.Vector3(0, 0, 0));

            drawCoords(t.scene);
            loadModel('dl/bf109e/bf109e.json', false, function (model) {
                t.scene.add(model);
            });
            addMainLight(t.scene);

            function render () {
                requestAnimationFrame(render);
                t.controls.update();
                t.renderer.render(t.scene, t.camera);
            }

            render();
        })();
    </script>

播放动画
========

既然模型已经 load 进来了，我们就来播放动画吧～这步其实很简单：

.. code-block:: javascript

    var animation = new THREE.Animation(model, model.geometry.animations[0]);
    animation.play();

    var clock = new THREE.Clock();
    function render() {
        requestAnimationFrame(render);
        // ....
        var delta = clock.getDelta();
        THREE.AnimationHandler.update(delta);
        // ....
    }
    render();

这里的 “animation.play()” 仅仅是将这个 `Animation`_ 对象标记为“播放中”，并没有
处理动画数据——具体的动画表现是由 `AnimationHandler`_ 完成的。需要注意的是到目
前为止 Three.js 的文档对 Animation 类的构造函数描述与实际代码都是 **不一致**
的：第二个参数是具体的动画数据，而不是动画数据的名称。这大概是因为 Three.js 中
处理动画的代码跟模型数据格式一样，最近更新很频繁，所以不久之后很可能又大不一样
了……

.. _Animation: http://threejs.org/docs/#Reference/Extras.Animation/Animation
.. _AnimationHandler: http://threejs.org/docs/#Reference/Extras.Animation/AnimationHandler

所有东西放在一起
================

为了让场景看起来更完整，我添加了简单的天空贴图和粒子效果：

.. raw:: html

    <p><div id="threejs-output-2"></div></p>

    <script>
        (function () {
            var t = initThree(
                    document.getElementById('threejs-output-2'), 
                    533, 300
                );
            t.renderer.setClearColor(0xcccccc);
            t.camera.position.set(1, 1, 1);
            t.camera.lookAt(new THREE.Vector3(0, 0, 0));

            var bf109_model;
            loadModel('dl/bf109e/bf109e.json', true, function (model) {
                bf109_model = model;
                t.scene.add(model);

				var animation = new THREE.Animation(model, model.geometry.animations[0]);
				animation.play();
            });
            addEnvMap(t.scene);
            addMainLight(t.scene);

            var clock = new THREE.Clock();
            var model_rot_dir = 1;
            var par_sys = drawParticles(t.scene, 1000, 200, 200, 400);

            function render () {
                requestAnimationFrame(render);

                t.controls.update();

                var delta = clock.getDelta();
                THREE.AnimationHandler.update(4 * delta);

                if (bf109_model) {
                    if (bf109_model.rotation.z >= 0.1 || bf109_model.rotation.z <= -0.1) {
                        model_rot_dir = -model_rot_dir;
                    }
                    var rot_delta = Math.min(0.001, Math.max(0.0005, (0.1 - Math.abs(bf109_model.rotation.z)) / 0.1 * 0.005));
                    bf109_model.rotation.z += model_rot_dir * rot_delta;
                }

                moveParticles(par_sys, 200, 200, 400, delta);

                t.renderer.render(t.scene, t.camera);
            }

            render();
        })();
    </script>
