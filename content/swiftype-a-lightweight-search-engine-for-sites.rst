Swiftype: 针对网站的轻量级搜索服务
##################################

:date: 2015-06-15 20:30
:tags: 搜索, blog
:category: Utilities
:slug: swiftype-a-lightweight-search-engine-for-sites
:author: Kay Z.
:lang: zh

很久没访问过 LinuxToy_ ，今天上去发现已经大改版，而且也 `迁移到 Pelican`_ 了。闲逛
之余发现页面上的搜索功能还挺好用的，就跟着边上 "Powered by Swiftype" 的链接，找到
`这家`_ 专门做网站搜索的小公司。

.. PELICAN_END_SUMMARY

他们的核心服务其实很简单：给你的网站建个索引，然后提供内容搜索服务。在这个静态网站
大行其道而且什么东西都流行外包的时代里，好像还真有这么个市场存在呢……根据
`TechCrunch的报道`_ ，这家公司年初的时候刚拿到一千多万美刀的融资。

我抱着试一试的心情注册了帐号，结果因为设置过程 **太简单** ，就直接加上了他们家的搜索按钮
（见页面右下角），前后不过五分钟……

其实这个服务跟 Google 的 `Custom Search Engine`_ 是一样一样的，说起来 Google 家的
免费功能还要强大一点，但是 Swiftype 在易用性上做得还算不错……什么时候 Google 要重视
这个市场了估计 Swiftype 就没戏了……

当然在中国国内可能还是有市场的，因为没有 Google ，另外一家搜索引擎又太恶心嘛……

.. _LinuxToy: https://linuxtoy.org/
.. _迁移到 Pelican: https://linuxtoy.org/archives/moving-to-pelican.html
.. _这家: https://swiftype.com/
.. _TechCrunch的报道: http://techcrunch.com/2015/03/05/swiftype-series-b/
.. _Custom Search Engine: https://cse.google.com/
