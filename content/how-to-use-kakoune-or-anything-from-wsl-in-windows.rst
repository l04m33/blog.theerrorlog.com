How to Use Kakoune (or Anything from WSL) in Windows
####################################################

:date: 2022-12-08 00:47
:tags: kakoune, helix, windows, wsl
:category: Utilities
:slug: how-to-use-kakoune-or-anything-from-wsl-in-windows
:author: Kay Z.

`Kakoune`_ is a versatile text editor that I can't recommend enough.
The user interface and plugin APIs are almost as stable as Vim.
Given its active development pace, few things in my dot files are
broken between releases, and I think that's really remarkable.

One of the aspects which make Kakoune so stable is that it delegates
most of the scripting tasks to POSIX shells. Auto-format the buffer?
Spawn a shell. Sort lines? Spawn a shell. Want a simple ``if``
statement? Spawn a shell.

And... that's exactly why Kakoune is not available (and never will
be) for Windows. It's a sad thing for fan boys like me, since I
still need to run Windows for... reasons.

I tried `Helix`_, but it was too unstable and too opinionated for a
text editor. It crashed randomly, and then threw away my whole
editing session. That's a risk I'd rather not take when doing serious
work.

.. _Kakoune: https://kakoune.org/
.. _Helix: https://helix-editor.com/

WSL to the Rescue
=================

Despite the funny name, WSL is actually a very decently designed
system. They've made reasonable ways to pass stuff around between
Windows and the things inside WSL. After some research work, I
found a way to use Kakoune to edit Windows files:

1. Make a Batch file named ``kak.bat`` [1]_:

   .. code-block:: bat

       @echo off

       if "%~1"=="-t" (
         set tmux_cmd=tmux new-session
       ) else (
         set tmux_cmd=
       )

       if "%~2"=="" goto NO_FILE

       for /f "usebackq tokens=*" %%a in (`wsl.exe wslpath -u '%~2'`) do wsl.exe %tmux_cmd% kak %%a

       goto DONE

       :NO_FILE

       wsl.exe %tmux_cmd% kak

       goto DONE

       :DONE

2. Make a shortcut that would launch the Batch file. Fill in the ``Target``:

   .. code-block:: bat

       wt.exe kak.bat -

   And if you want to start a Tmux session:

   .. code-block:: bat

       wt.exe kak.bat -t

3. Profit! You can now use the shortcut to launch empty Kakoune
   sessions. To edit a file in Windows, drag & drop it onto the
   shortcut. You can even add Kakoune to Windows' SendTo menu by
   ``Win + r`` --> ``shell:SendTo`` and then drop the shortcut
   there.

.. [1] Please don't judge me. This is literally the first Batch file
       I have ever written.

Now, whenever I'm editing a text file in Windows, there's a whole
Linux virtual machine running in the background. Still a good
trade-off for a decent text editor, eh?
