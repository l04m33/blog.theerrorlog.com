Resurrection
############

:date: 2018-12-13 20:19
:tags: blog, life
:category: Blog
:slug: resurrection
:author: Kay Z.

It's been a while since I blogged here. Two years, to be
more precise. I've been caught up in things, especially 
the fact that I'm a father now 😎. I still maintained
this little site during the period of silence though,
taking my scarce free time to make sure it kept running.

Earlier this month, I decided to start blogging again, so
here I am. And to hold a welcoming ceremony for myself, I
created `this new theme`_ for Pelican. Let's see how this
*resurrection* works out.

.. _this new theme: https://bitbucket.org/l04m33/pelican-errorlog
