Title: coverage.py的覆盖率统计原理
Date: 2014-10-22 12:00
Category: Utilities
Tags: python, coverage.py
Slug: how-coverage-py-produce-coverage-statistics
Author: Kay Z.
Lang: zh

在Erlang下，用[rebar][1]可以在运行单元测试的同时调用[cover][2]模块，从而得
到单元测试的覆盖率，这是很方便的一个功能。最近发现Python下的[nose][3]也可
以通过`--with-coverage`选项达到类似的效果，而这个选项实际上是调用了
[coverage.py][4]. 出于好奇，我研究了一下coverage.py的工作原理。

[1]: https://github.com/basho/rebar
[2]: http://www.erlang.org/doc/man/cover.html
[3]: https://nose.readthedocs.org/en/latest/
[4]: http://nedbatchelder.com/code/coverage/

<!-- PELICAN_END_SUMMARY -->

## coverage.py的使用方法 ##

抄一下coverage.py主页的例子：

1. 用`coverage`命令运行你的程序

        :::bashsession
        $ coverage run my_program.py arg1 arg2 
        blah blah ..your program's output.. blah blah

2. 生成报告

        #!bashsession
        $ coverage report -m
        Name                      Stmts   Miss  Cover   Missing
        -------------------------------------------------------
        my_program                   20      4    80%   33-35, 39
        my_other_module              56      6    89%   17-23
        -------------------------------------------------------
        TOTAL                        76     10    87%

3. 或者，生成更漂亮的HTML报告

        :::bashsession
        $ coverage html

好吧对于一个开发工具来说，`coverage`算是挺成功的，因为够傻瓜。和
`nosetests`搭配使用的时候甚至更简单，加一个参数就可以了，“猴子都能学
会”……

既然这里有个脚本入口`coverage`，我们就从这里开始吧～

## 工作原理 ##

首先从[源码库][5]把代码clone下来，观察一下`setup.py`：

    #!python
    scripts = [                                                                      
        'coverage = coverage:main',                                                  
        'coverage%d = coverage:main' % sys.version_info[:1],                         
        'coverage-%d.%d = coverage:main' % sys.version_info[:2],                     
        ]

这里说明，`coverage`命令其实是调用了`coverage`模块下的`main`函数，根据这个
函数的内容——为了简洁起见这里省略掉一堆处理命令行参数的代码——我们可以跟踪到
`coverage/control.py`文件中的`Coverage`类。

这个类包含了所有从数据采集到生成报告的代码，我们只关心它如何采集到程序执行
数据就好了。而“采集数据”这个操作，通过这里的源码可以推断出，是由
`coverage/collector.py`中的`Collector`类和`PyTracer`类，又或者是`CTracer`
类合作完成的。

`PyTracer`和`CTracer`其实在逻辑上是等价的，只不过一个是纯Python实现，而另
一个是C语言实现。为什么会这样呢？我们先继续看下去……

既然这两个类是一样的，我们还是来看稍微漂亮一点的Python版本吧。`PyTracer`
的`start`函数有句docstring说，这个函数是用来“Return a Python function suitable
for use with sys.settrace()”的。

好吧，真相大白了，coverage.py利用了Python虚拟机的trace机制。我怎么就没想
到呢？

[5]: https://bitbucket.org/ned/coveragepy

## sys.settrace(...) ##

这个函数的文档在[这里][6]，所以参数和用法说明什么的我就省略了。实际上
这是Python程序调试机制的核心——几乎每种虚拟机或者操作系统，都有类似的
机制，用于干预其上执行的程序，可以认为这是给对应的调试器开的“后门”。
例如Erlang下有[erlang:trace(...)][7]，POSIX系统下也有[ptrace][8]；
Erlang的[dbg][9]模块正是基于erlang:trace(...)的，而gdb也是在ptrace基
础上工作的。

大家都知道调试器都是牛逼到可以把运行中的程序拆开再装回去的，它们依赖
的trace机制自然也是大杀器。但是这货通常要启动各种钩子（Hook），会严
重拖慢被trace的程序的运行速度，所以一般不会用在生产环境中。

这也就解释了为什么coverage.py里会有两个tracer：tracer代码基本上在
Python虚拟机的每步执行中都要被调用一遍，所以有一个C语言实现是能有效
提高效率的。那为什么还需要Python实现？这是为了支持CPython以外的虚拟
机，像PyPy、Jython之类。

[6]: https://docs.python.org/3.4/library/sys.html#sys.settrace
[7]: http://erlang.org/doc/man/erlang.html#trace-3
[8]: http://en.wikipedia.org/wiki/Ptrace
[9]: http://erlang.org/doc/man/dbg.html

## 其他利用trace机制的有趣玩意 ##

[pycallgraph][10]能生成Python程序中的函数调用关系图（[Call Graph][11]）。

另外我在很～久之前用ptrace做了一个更改`cd`命令行为的[小玩具][12] :)

[10]: https://pycallgraph.readthedocs.org/en/master/
[11]: http://en.wikipedia.org/wiki/Call_graph
[12]: https://github.com/l04m33/watcher_trace
